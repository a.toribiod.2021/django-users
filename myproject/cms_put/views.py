from django.shortcuts import render
from django.http import HttpResponse
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout
from django.shortcuts import redirect

# Create your views here.


def index(request):
    contenido = Contenido.objects.all()
    return render(request, 'home.html', {'contenido': contenido})


@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        if request.user.is_authenticated:
            valor = request.body.decode('utf-8')
            c = Contenido(clave=llave, valor=valor)
            c.save()
            respuesta = Contenido.objects.get(clave=llave).valor
            return HttpResponse(respuesta)
        else:
            return HttpResponse("Not logged in. Can't change content. " +
                                "<a href='/login/'>Login</a>")
    else:
        try:
            respuesta = Contenido.objects.get(clave=llave).valor
        except Contenido.DoesNotExist:
            respuesta = 'No existe contenido para la clave ' + llave
        return HttpResponse(respuesta)


def loggedIn(request):
    if request.user.is_authenticated:
        logged = ("Logged in as " + request.user.username +
                  ". <a href='/cms_put/logout'>Logout</a>")
    else:
        logged = "Not logged in. <a href='/login/'>Login</a>"
    return HttpResponse(logged)


def logout_view(request):
    logout(request)
    return redirect("/cms_put/")
